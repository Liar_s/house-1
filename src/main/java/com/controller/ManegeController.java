package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.domain.Teacher;
import com.service.ManegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ManegeController {
    @Autowired
    private ManegeService manegeService;

    @ResponseBody
    @RequestMapping("/showUser")
    public Teacher showUser(@RequestBody JSONObject jsonObject){
        return manegeService.showUser(jsonObject);
    }

    @ResponseBody
    @RequestMapping("/changeMessage")
    public Teacher changeMessage(@RequestBody JSONObject jsonObject){
        return manegeService.changeMessage(jsonObject);
    }

    @ResponseBody
    @RequestMapping("/showStudent")
    public List showStudent(@RequestBody JSONObject jsonObject){
        return manegeService.showStudent(jsonObject);
    }

    @ResponseBody
    @RequestMapping("/getCourse")
    public List getCourse(@RequestBody JSONObject jsonObject){return manegeService.getCourse(jsonObject);}

    @ResponseBody
    @RequestMapping("/delete")
    public List delete(@RequestBody JSONObject jsonObject){
        return manegeService.delete(jsonObject);}

    @ResponseBody
    @RequestMapping("/modisave")
    public List modifySave(@RequestBody JSONObject jsonObject){
        return manegeService.modifySave(jsonObject);}

    @ResponseBody
    @RequestMapping("/addsave")
    public List addSave(@RequestBody JSONObject jsonObject){
        return manegeService.addSave(jsonObject);}

}
