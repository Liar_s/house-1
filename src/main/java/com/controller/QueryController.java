package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.service.QueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class QueryController {
    @Autowired
    private QueryService queryService;

    @ResponseBody
    @RequestMapping("/showGrade")
    public List showGrade(@RequestBody JSONObject jsonObject){
        return queryService.showGrade(jsonObject);
    }
}
