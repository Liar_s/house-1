package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.service.GetInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GetInController {

    @Autowired
    private GetInService getInService;

    @ResponseBody
    @RequestMapping("/sign")
    public String sign(@RequestBody JSONObject jsonObject){
        return getInService.sign(jsonObject);
    }

    @ResponseBody
    @RequestMapping("/login")
    public String login(@RequestBody JSONObject jsonObject){
        return getInService.login(jsonObject);
    }
}
