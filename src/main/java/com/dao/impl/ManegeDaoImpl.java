package com.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.dao.ManegeDao;
import com.domain.Student;
import com.domain.Teacher;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

public class ManegeDaoImpl implements ManegeDao {
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Teacher showUser(JSONObject jsonObject) {
        String sql="select * from Teacher where tea_name=?";
        Teacher teacher=jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<Teacher>(Teacher.class),jsonObject.getString("username"));
        return teacher;
    }

    public List showStudent(JSONObject jsonObject) {
        String sql="select Student.stu_id,Student.stu_name,SC.bpoint,SC.tpoint from Course join SC on SC.cou_id=Course.cou_id join Student on Student.stu_id=SC.stu_id WHERE Course.cou_name=?";
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,jsonObject.getString("course_name"));
        return rows;
    }

    public List getCourse(JSONObject jsonObject) {
        String sql="select * from Course where cou_tea=?";
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,jsonObject.getString("cou_tea"));
        return rows;
    }

    public List delete(JSONObject jsonObject) {
        String sql="select Student.stu_id,Student.stu_name,SC.bpoint,SC.tpoint from Course join SC on SC.cou_id=Course.cou_id join Student on Student.stu_id=SC.stu_id WHERE Course.cou_id=?";
        String del_sql="delete from SC where cou_id=? and stu_id=?";
        jdbcTemplate.update(del_sql,jsonObject.getString("cou_id"),jsonObject.getString("stu_id"));
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,jsonObject.getString("cou_id"));
        return rows;
    }

    public List modifySave(JSONObject jsonObject) {
        String sql="select Student.stu_id,Student.stu_name,SC.bpoint,SC.tpoint from Course join SC on SC.cou_id=Course.cou_id join Student on Student.stu_id=SC.stu_id WHERE Course.cou_id=?";
        String modify_sql="update SC set bpoint=?,tpoint=? where cou_id=? and stu_id=?";
        jdbcTemplate.update(modify_sql,jsonObject.getString("bpoint"),jsonObject.getString("tpoint"),jsonObject.getString("cou_id"),jsonObject.getString("stu_id"));
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,jsonObject.getString("cou_id"));
        return rows;
    }

    public List addSave(JSONObject jsonObject) {
        String sql="select Student.stu_id,Student.stu_name,SC.bpoint,SC.tpoint from Course join SC on SC.cou_id=Course.cou_id join Student on Student.stu_id=SC.stu_id WHERE Course.cou_id=?";
        String add_sql="insert into SC(stu_id,cou_id,bpoint,tpoint) VALUES (?,?,?,?)";
        jdbcTemplate.update(add_sql,jsonObject.getString("stu_id"),jsonObject.getString("cou_id"),jsonObject.getString("bpoint"),jsonObject.getString("tpoint"));
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,jsonObject.getString("cou_id"));
        return rows;
    }

    public Teacher changeMessage(JSONObject jsonObject) {
        String change_sql="update Teacher set tea_sex=?,tea_dep=?,tea_phone=? where tea_name=?";
        jdbcTemplate.update(change_sql,jsonObject.getString("tea_sex"),jsonObject.getString("tea_dep"),jsonObject.getString("tea_phone"),jsonObject.getString("tea_name"));
        String sql="select * from Teacher where tea_name=?";
        Teacher teacher=jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<Teacher>(Teacher.class),jsonObject.getString("tea_name"));
        return teacher;
    }
}
