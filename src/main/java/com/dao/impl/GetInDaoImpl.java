package com.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.dao.GetInDao;
import com.domain.Student;
import com.domain.Teacher;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class GetInDaoImpl implements GetInDao {
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String sign(JSONObject jsonObject) {
        if (jsonObject.getString("radioValue").equals("老师")){
            String sign="insert into Teacher (tea_name,tea_psw,tea_phone) values(?,?,?)";
            String check="select * from Teacher where tea_phone=?";
            if(jdbcTemplate.query(check, new BeanPropertyRowMapper<Teacher>(Teacher.class),jsonObject.getString("phoneValue")).isEmpty()
                    &&jdbcTemplate.update(sign,jsonObject.getString("nameValue"),jsonObject.getString("pswValue"),jsonObject.getString("phoneValue"))>0){
                return "succeed";
            }
            else {
                return "error";
            }
        }
        else{
                String sign="insert into Student (stu_name,stu_psw,stu_phone) values(?,?,?)";
                String check="select * from Student where stu_phone=?";
                if(jdbcTemplate.query(check, new BeanPropertyRowMapper<Student>(Student.class),jsonObject.getString("phoneValue")).isEmpty()
                        &&jdbcTemplate.update(sign,jsonObject.getString("nameValue"),jsonObject.getString("pswValue"),jsonObject.getString("phoneValue"))>0){
                    return "succeed";
                }
                else {
                    return "error";
                }
        }

    }

    public String login(JSONObject jsonObject) {
        if (jsonObject.getString("radioValue").equals("老师")){
            String check="select * from Teacher where tea_name=?";
            if(!jdbcTemplate.query(check, new BeanPropertyRowMapper<Teacher>(Teacher.class),jsonObject.getString("nameValue")).isEmpty()&&
                    jdbcTemplate.queryForObject("select * from Teacher where tea_name=?",new BeanPropertyRowMapper<Teacher>(Teacher.class),jsonObject.getString("nameValue"))
                            .getTea_psw().equals(jsonObject.getString("pswValue"))){
                return "succeed";
            }
            else {
                return "error";
            }
        }
        else{
            String check="select * from Student where stu_name=?";
            if(!jdbcTemplate.query(check, new BeanPropertyRowMapper<Student>(Student.class),jsonObject.getString("nameValue")).isEmpty()&&
                    jdbcTemplate.queryForObject("select * from Student where stu_name=?",new BeanPropertyRowMapper<Student>(Student.class),jsonObject.getString("nameValue"))
                            .getStu_psw().equals(jsonObject.getString("pswValue"))){
                return "succeed";
            }
            else {
                return "error";
            }
        }
    }
}
