package com.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.dao.QueryDao;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

public class QueryDaoImpl implements QueryDao {
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List showGrade(JSONObject jsonObject) {
        String sql="select Course.cou_name,Teacher.tea_name,SC.bpoint,SC.tpoint from Course join SC on SC.cou_id=Course.cou_id join Student on Student.stu_id=SC.stu_id join Teacher on Teacher.tea_name=Course.cou_tea WHERE Student.stu_name=?";
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,jsonObject.getString("stu_name"));
        return rows;
    }
}
