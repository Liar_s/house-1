package com.dao;

import com.alibaba.fastjson.JSONObject;

public interface GetInDao {

    String sign(JSONObject jsonObject);

    String login(JSONObject jsonObject);
}
