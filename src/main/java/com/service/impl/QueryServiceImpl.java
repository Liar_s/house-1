package com.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dao.QueryDao;
import com.service.QueryService;

import java.util.List;

public class QueryServiceImpl implements QueryService {
    private QueryDao queryDao;

    public void setQueryDao(QueryDao queryDao) {
        this.queryDao = queryDao;
    }

    public List showGrade(JSONObject jsonObject) {
        return queryDao.showGrade(jsonObject);
    }
}
