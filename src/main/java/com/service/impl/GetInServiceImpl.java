package com.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dao.GetInDao;
import com.dao.impl.GetInDaoImpl;
import com.service.GetInService;
import org.springframework.beans.factory.annotation.Autowired;

public class GetInServiceImpl implements GetInService {
    private GetInDao getInDao;
    public void setGetInDao(GetInDao getInDao) {
        this.getInDao = getInDao;
    }

    public String sign(JSONObject jsonObject) {
        return getInDao.sign(jsonObject);
    }

    public String login(JSONObject jsonObject) {
        return getInDao.login(jsonObject);
    }

}
