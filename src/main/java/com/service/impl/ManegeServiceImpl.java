package com.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.dao.ManegeDao;
import com.domain.Teacher;
import com.service.ManegeService;

import java.util.List;

public class ManegeServiceImpl implements ManegeService {
    private ManegeDao manegeDao;

    public void setManegeDao(ManegeDao manegeDao) {
        this.manegeDao = manegeDao;
    }

    public Teacher showUser(JSONObject jsonObject) {

        return  manegeDao.showUser(jsonObject);
    }

    public List showStudent(JSONObject jsonObject) {
        return manegeDao.showStudent(jsonObject);
    }

    public List getCourse(JSONObject jsonObject) {
        return manegeDao.getCourse(jsonObject);
    }

    public List delete(JSONObject jsonObject) {
        return manegeDao.delete(jsonObject);
    }

    public List modifySave(JSONObject jsonObject) {
        return manegeDao.modifySave(jsonObject);
    }

    public List addSave(JSONObject jsonObject) {
        return manegeDao.addSave(jsonObject);
    }

    public Teacher changeMessage(JSONObject jsonObject) {
        return manegeDao.changeMessage(jsonObject);
    }
}
