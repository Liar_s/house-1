package com.service;

import com.alibaba.fastjson.JSONObject;

public interface GetInService {
   public String sign(JSONObject jsonObject);

   public String login(JSONObject jsonObject);
}
