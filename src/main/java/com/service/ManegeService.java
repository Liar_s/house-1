package com.service;

import com.alibaba.fastjson.JSONObject;
import com.domain.Teacher;

import java.util.List;

public interface ManegeService {
    Teacher showUser(JSONObject username);

    List showStudent(JSONObject jsonObject);

    List getCourse(JSONObject jsonObject);

    List delete(JSONObject jsonObject);

    List modifySave(JSONObject jsonObject);

    List addSave(JSONObject jsonObject);

    Teacher changeMessage(JSONObject jsonObject);
}
