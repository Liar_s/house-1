package com.domain;

public class Student {
    private int stu_id;
    private String stu_name;
    private String stu_sex;
    private String stu_age;
    private String stu_phone;
    private String stu_psw;

    public String getStu_psw() {
        return stu_psw;
    }

    public void setStu_psw(String stu_psw) {
        this.stu_psw = stu_psw;
    }

    public int getStu_id() {
        return stu_id;
    }

    public void setStu_id(int stu_id) {
        this.stu_id = stu_id;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_sex() {
        return stu_sex;
    }

    public void setStu_sex(String stu_sex) {
        this.stu_sex = stu_sex;
    }

    public String getStu_age() {
        return stu_age;
    }

    public void setStu_age(String stu_age) {
        this.stu_age = stu_age;
    }

    public String getStu_phone() {
        return stu_phone;
    }

    public void setStu_phone(String stu_phone) {
        this.stu_phone = stu_phone;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stu_id=" + stu_id +
                ", stu_name='" + stu_name + '\'' +
                ", stu_sex='" + stu_sex + '\'' +
                ", stu_age='" + stu_age + '\'' +
                ", stu_phone='" + stu_phone + '\'' +
                ", stu_psw='" + stu_psw + '\'' +
                '}';
    }
}
