package com.domain;

public class Teacher {
    private int tea_id;
    private String tea_name;
    private String tea_sex;
    private String tea_dep;
    private String tea_phone;
    private String tea_psw;

    public String getTea_psw() {
        return tea_psw;
    }

    public void setTea_psw(String tea_psw) {
        this.tea_psw = tea_psw;
    }

    public int getTea_id() {
        return tea_id;
    }

    public void setTea_id(int tea_id) {
        this.tea_id = tea_id;
    }

    public String getTea_name() {
        return tea_name;
    }

    public void setTea_name(String tea_name) {
        this.tea_name = tea_name;
    }

    public String getTea_sex() {
        return tea_sex;
    }


    public void setTea_sex(String tea_sex) {
        this.tea_sex = tea_sex;
    }

    public String getTea_dep() {
        return tea_dep;
    }

    public void setTea_dep(String tea_dep) {
        this.tea_dep = tea_dep;
    }

    public String getTea_phone() {
        return tea_phone;
    }

    public void setTea_phone(String tea_phone) {
        this.tea_phone = tea_phone;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "tea_id=" + tea_id +
                ", tea_name='" + tea_name + '\'' +
                ", tea_sex='" + tea_sex + '\'' +
                ", tea_dep='" + tea_dep + '\'' +
                ", tea_phone='" + tea_phone + '\'' +
                ", tea_psw='" + tea_psw + '\'' +
                '}';
    }
}
